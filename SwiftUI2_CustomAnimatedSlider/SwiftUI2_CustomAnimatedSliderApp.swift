////  SwiftUI2_CustomAnimatedSliderApp.swift
//  SwiftUI2_CustomAnimatedSlider
//
//  Created on 17/02/2021.
//  
//

import SwiftUI

@main
struct SwiftUI2_CustomAnimatedSliderApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
